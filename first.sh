#!/usr/bin/env sh

FILE=${TEAMCITY_PROJECT_NAME}-${TEAMCITY_BUILDCONF_NAME}-$(cat version).${BUILD_NUMBER}

mkdir stdout -p
python3 -m unittest -v &> stdout/log-${FILE}.txt
