#!/usr/bin/env sh

FILE=${TEAMCITY_PROJECT_NAME}-${TEAMCITY_BUILDCONF_NAME}-$(cat version).${BUILD_NUMBER}

TARGET=$(echo -n $(grep 'FAIL' stdout/log-${FILE}.txt) | wc -m)

if [ $TARGET -eq 0 ]
then
	echo "Все впорядке" > stdout/stdout-${FILE}.txt
else
	echo "Стоит проверить тесты" > stdout/stdout-${FILE}.txt
fi
